# Utils-Glab

This project is an example that show how can you organize your gitlab in a way that I think it's good ;)

# How can I found the examples ?

You can see the [Issues](https://gitlab.com/leisiamedeiros/utils-glab/-/issues) of this repository as well the 
[Requirements](https://gitlab.com/leisiamedeiros/utils-glab/-/requirements_management/requirements), the [Milestones](https://gitlab.com/leisiamedeiros/utils-glab/-/milestones/1) and the [Test Cases](https://gitlab.com/leisiamedeiros/utils-glab/-/quality/test_cases) to inspire you :smiley: 
 
